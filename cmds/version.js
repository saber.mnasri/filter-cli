const { version } = require('../package.json')

/**
 * print version from package.json
 */
module.exports = () => {
  console.log(`v${version}`)
}