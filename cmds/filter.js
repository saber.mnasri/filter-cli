const { data } = require('../data')

/**
 *  filter a list of animals containing a pattern. 
 *  animals containing the pattern passed as argument (e.g. `ry`) are displayed. 
 *  The order is kept intact.
 *
 * @param {*} option object containing flag and pattern to filter
 */
module.exports = (option) => {

    const { value } = option;

    return data.reduce((acc, country) => {
            if (country.people && country.people.length > 0) {
                const people = country.people.reduce((accp, p) => {
                    if (p.animals && p.animals.length > 0) {
                        //const animals = p.animals.filter(a => a.name.includes(value))
                        const animals = p.animals.reduce((acca, animal) => {
                            if (animal.name.toLocaleLowerCase().includes(value.toLocaleLowerCase())) {
                                acca.push({...animal});
                            }
                            return acca;
                        },[],);
                        if (animals.length > 0) {
                            accp.push({...p, animals});
                        }
                    }
                    return accp;
                },[],);
                if (people.length > 0) {
                acc.push({...country, people});
                }
            }
            return acc
    },[],);

}