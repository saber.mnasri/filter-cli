const { data } = require('../data')

/**
 * function to print the counts of People and Animals by counting the number
 * of children and appending it in the name, eg. `Satanwi [2]`.
 *
 */
module.exports = () => {

    return data.reduce((acc, country) => {
            if (country.people && country.people.length > 0) {
                const people = country.people.reduce((accp, p) => {
                    if (p.animals && p.animals.length > 0) {
                        //const animals = p.animals.filter(a => a.name.includes(value))
                        const animals = p.animals.reduce((acca, animal) => {
                            acca.push({...animal});
                            return acca;
                        },[],);
                        if (animals.length > 0) {
                            accp.push({
                                name: p.name + " ["+ animals.length +"]", 
                                animals
                            });
                        }
                    }
                    return accp;
                },[],);
                if (people.length > 0) {
                    acc.push({
                        name: country.name + " ["+ people.length +"]", 
                        people
                    });
                }
            }
            return acc
    },[],);

}