/**
 * print help message
 */
module.exports = () => {
  console.log(`
    filter-cli <option>

    options:
      --filter .............. filter a list of elements (e.g. --filter=yx)
      --count ............... counts a list of elements (e.g. --count)
      --help ................ show help menu (e.g. --help)
      --version ............. show version (e.g. --version)
    `);
}