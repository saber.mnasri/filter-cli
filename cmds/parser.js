/**
 * Reads the `process.argv` values and converts valid arguments into a 
 * key-value pairs object (e.g. { flag: 'filter', value: 'nop' })
 *
 * @param {*} argv argument array to parse
 * @returns {options}  array of key-value pairs object
 */
module.exports = function (argv) {

    const flagFiler = 'filter';
    const acceptedFlags = ['count','version','help'];
    let options = [];

    // if at least one option is provided
    if (argv.length >= 0) {

        argv.map((arg) => {

            // check for
            // - long options : --flag=value or --flag
            // - short option : -flag=value or -flag
            // ignore other cases

            if (arg.substr(0,1) === '-' && arg.length > 2){

                // value flag --flag=value (e.g. --filter=xy)
                // OR
                // flag --flag (e.g. --count, --version)

                if(arg.substr(1,1) === '-'){

                    if (arg.indexOf('=') > 2){
                        // value flag --flag=value
                        var argstr = arg.substr(2);

                        // get flag and value
                        argstr = argstr.split('=');

                        //check if flag is filter
                        if (flagFiler === argstr[0].toLowerCase() && argstr[1].length!=0){
                            options.push({
                                flag: argstr[0].toLowerCase(),
                                value: argstr[1]
                            });
                        }

                    } else {
                        // flag without value --flag
                        var argstr = arg.substr(2);

                        //check flag
                        if (acceptedFlags.includes(argstr.toLowerCase()))
                            options.push({
                                flag: argstr.toLowerCase(),
                                value: true
                            });

                    }

                }
            }

        });
    }
    
    return options;
};

