var FilterCli = require('../cli.js');
const assert = require('assert')

describe("handle options", () => {

    test('should return first valid option', () => {
        const argv = ['fit=test','--filter=nop','--count'];
        const filterCli = new FilterCli(argv);
        //const option = filterCli.option;
        assert.deepEqual(filterCli.option,{ flag: 'filter', value: 'nop' });
    });

    test('should return help if no valid option', () => {
        const argv = ['opt=test'];
        const filterCli = new FilterCli(argv);
        //const option = filterCli.option;
        assert.deepEqual(filterCli.option,{});
    });
});