var count = require('../cmds/count.js');
var { mockData, mockFailedData } = require('./count.data.js');
const assert = require('assert')


describe("add count to children", () => {
    test('Should add count to children.', () => {

        const countData = count()

        assert.deepEqual(countData,mockData);
    });
    test('Should fail to add count to children.', () => {

        const countData = count()

        assert.notDeepEqual(countData,mockFailedData);
    });
});