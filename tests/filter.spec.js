var filter = require('../cmds/filter.js');
const assert = require('assert')


describe("filter data", () => {
    test('Should filter a list of animals containing a pattern.', () => {

        const mockData = [
            {
                "name": "Dillauti",
                "people": [
                    {
                        "name": "Philip Murray",
                        "animals": [
                            {
                                "name": "Xenops"
                            }
                        ]
                    }
                ]
            }
        ];

        const filtered = filter({ flag: 'filter', value: 'nop' });

        assert.deepEqual(filtered,mockData);
    });
});