var parse = require('../cmds/parser.js');
const assert = require('assert')


describe("parse arguments", () => {
test('basic arguments from process.argv', () => {
    const argv = ['node.exe','app.js','--filter=nop','-test'];
    const options = parse(argv);

    assert.deepEqual(options,[{ flag: 'filter', value: 'nop' }])
});

test('arg with no flag', () => {
    const argv = ['node.exe','app.js'];
    const options = parse(argv);
    assert.deepEqual(options,[])
});

test('arg with not accepted flag', () => {
    const argv = ['node.exe','app.js','--myflag=test'];
    const options = parse(argv);
    assert.deepEqual(options,[])
});

test('arg with filter flag', () => {
    const argv = ['node.exe','app.js','--filter=test'];
    const options = parse(argv);

    assert.deepEqual(options,[{ flag:'filter', value:'test'}])
});

test('should return accepted flags', () => {
    const argv = ['node.exe','app.js','--filter=nop','-test','--count'];
    const options = parse(argv);

    assert.deepEqual(options,[
        { flag: 'filter', value: 'nop' },
        { flag: 'count', value: true }]);
});

test('error: invalid filter flag', () => {
    const argv = ['node.exe','app.js','--filter='];
    const options = parse(argv);

    assert.deepEqual(options,[])
});

test('arg with count flag', () => {
    const argv = ['node.exe','app.js','--count'];
    const options = parse(argv);

    assert.deepEqual(options, [{ flag:'count', value:true}])
});

test('error: invalid option', () => {
    const argv = ['node.exe','app.js','--foo=1234'];
    const options = parse(argv);

    assert.deepEqual(options,[])
});
});