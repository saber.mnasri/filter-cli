# Filter-cli

Filter-cli is a command-line interface. His goals are:
 - Filter a list of elements containing a pattern.
 - print the counts of People and Animals by counting the number of children and appending it in the name, eg. `Satanwi [2]`.


## Usage

```shell script
$ node app.js --help
Usage: filter-cli <option>

options:
  --filter .............. filter a list of elements (e.g. --filter=yx)
  --count ............... counts a list of elements (e.g. --count)
  --help ................ show help menu (e.g. --help)
  --version ............. show version (e.g. --version)
```

## Filter

Filter a list of animals containing a pattern. Only animals containing the pattern passed as argument (e.g. `ry`) are displayed
Sample of running the command, and its output:

```shell script
$ node app.js --filter=ry
[
    {
        name: 'Uzuzozne',
        people: [
            {
                name: 'Lillie Abbott',
                animals: [
                    { name: 'John Dory' }
                ]
            }
        ]
    },
  {
    name: 'Satanwi',
    people: [
      {
        name: 'Anthony Bruno',
        animals: [
          {
            name: 'Oryx'
          }
        ]
      }
    ]
  }
]
```

## Count

Print the counts of People and Animals by counting the number of children and appending it in the name, eg. `Satanwi [2]`.

Sample of running the command, and its output:

```shell script
node app.js --count
[ 
    { 
        name: 'Dillauti [5]',
        people: [
            {
                name: 'Winifred Graham [6]',
                animals: [
                    { name: 'Anoa' },
                    { name: 'Duck' },
                    { name: 'Narwhal' },
                    { name: 'Badger' },
                    { name: 'Cobra' },
                    { name: 'Crow' } 
                ]
            },
            { 
                name: 'Blanche Viciani [8]',
                animals: [
                    { name: 'Barbet' },
                    { name: 'Rhea' },
                    { name: 'Snakes' },
                    { name: 'Antelope' },
                    { name: 'Echidna' },
                    { name: 'Crow' },
                    { name: 'Guinea Fowl' },
                    { name: 'Deer Mouse' } 
                ]
            },
      ...
...
]
```

## Version

Print version of filter-cli

## Help

Print help message to user. This message is displayed when the option written is not correct!

## License

MIT ©2023
