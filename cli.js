
'use strict';
const parse = require('./cmds/parser');
const filter = require('./cmds/filter');
const count = require('./cmds/count');
const printVersion = require('./cmds/version');
const printHelp = require('./cmds/help');
const { isEmptyObj } = require('./utils/obj');

/**
 * class to handle user commands and options
 */
class FilterCli {

    // options list
    argv;

    // valid option
    option


    constructor(args=[]) {
        //pars arguments and get options
        this.argv = (args.length!=0 ? args : process.argv.slice(2));

        //pars arguments and return valid options
        const options = parse(this.argv);

        // get first valid option
        this.option = this.selectOption(options);

        //handle option
        this.handleOption();
    }


    /**
     * handle option and forward to adequate function
     * @readonly
     * @memberof FilterCli
     * @returns option first valid option if list is not empty
     */
    handleOption(){

        const opt = !isEmptyObj(this.option) ? this.option : { flag:'help', value:true };

        switch (opt.flag) {

            case 'filter':
                const filtered = filter(opt);
                console.log(filtered.length > 0 ? JSON.stringify(filtered, null, 4):'');
                break;

            case 'count':
                const counted = count();
                console.log(JSON.stringify(counted, null, 4));
                break;

            case 'version':
                printVersion();
                break;

            case 'help':
                printHelp();
                break;

            default:
                printHelp();
                break;
        }
    }

    /**
     * Return first valid option if options list is not empty otherwise 
     * returns help option
     * @readonly
     * @memberof FilterCli
     * @param {*} options options array to select from
     * @returns option first valid option if list is not empty
     */
    selectOption(options){
        // check if options is empty or contains empty object ? 
        // if so, print help message
        return options.length > 0 ? options[0] : {}
    }

}

module.exports = FilterCli;