/**
 * check if object is empty
 * @param {*} obj to check
 * @returns boolean 
 */
function isEmptyObj(obj) {
    return Object.keys(obj).length === 0 && obj.constructor === Object;
}

module.exports = {
    isEmptyObj
}